# README #

This repository consists files configurate semantic-ui css/js-framework for [project Mediaboard](https://bitbucket.org/alexshin/media-freelance-board)

### Requirements ###

* Installed nodejs
* Installed gulp

### How to build distribution? ###

Type the next commands to your rerminal app:

```
#!bash

# Create directory for build sources and to move into it
mkdir ~/temporary_project_root
cd ~/temporary_project_root

# Get all packages for build semanticui (input default values)
npm install semantic-ui --save
cd semantic/

# Get configuration files from this repo
rm -rf ./src
git clone git@bitbucket.org:mediaboard/mediaboard-semanticui.git ./src

# Build sources
gulp build

```

### Additional information ###

You sould use [Semantic-UI docs](http://semantic-ui.com/introduction/getting-started.html) to get all needed information

Enjoy it!